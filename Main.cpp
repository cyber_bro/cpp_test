#include <iostream>

using namespace std;

class Animal
{
public:
	virtual ~Animal() = default;

	void virtual Voice()
	{
		cout << "A";
	}
};

class Dog final : public Animal
{
public:
	void Voice() override
	{
		cout << "Woof!";
	}
};


class Cat final : public Animal
{
public:
	void Voice() override
	{
		cout << "Meow!";
	}
};


class Duck final : public Animal
{
public:
	void Voice() override
	{
		cout << "Quack!";
	}
};

int main()
{
	Animal* Animals[] = {new Dog, new Cat, new Duck};

	for (const auto A : Animals)
	{
		A->Voice();
	}
}
